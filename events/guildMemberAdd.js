const sql = require("sqlite");
sql.open("./muteDatabase.sqlite");
exports.run = function(bot, member) {
  sql.get(`SELECT * FROM muteDatabase WHERE userID = ${member.id}`).then(function(row) {
    let role = member.guild.roles.find("name", "Axel-Mute");
    if (row) {
      member.addRole(role.id).catch(error => console.error(error.stack));
      member.send("Nice attempt to bypass the mute :)");
    } else return;
  }).catch(error => console.error(error));

  bot.on('guildMemberAdd', member => {
    if (member.guild.id != 200654805125955586) return;
    var role = member.guild.roles.find('name', 'NotAgreed');
    member.addRole(role)
  });
  
  bot.on('guildMemberAdd', msg => {
      if (msg.guild.id != 200654805125955586) return;
      msg.send({embed: {
      color: 0xff0000,
      author: {
        name: msg.client.user.username,
        icon_url: msg.client.user.avatarURL
      },
      title: "Welcome to XenMediaGroup!",
      description: "Thank you for joining our discord. Go check out our rules at #agree then type >agree! Some of the things we offer here are:",
      fields: [{
          name: "Axel Support",
          value: "We have support for our custom bot named Axel!"
        },
        {
          name: "Youtube Updates",
          value: "Here we post updates for our [YouTube Channel](https://www.youtube.com/channel/UCsqKNIZJ_p2SnzUSLOJttUA)."
        },
        {
          name: "Great Community",
          value: "Here we have a very great community with lots of nice people!"
        }
      ],
      timestamp: new Date(),
      footer: {
        icon_url: msg.client.user.avatarURL,
        text: "Welcome Message"
      }
    }
  });
  });

};

