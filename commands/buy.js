exports.run = function(bot, msg, args) {
    const codetype = args[0];
    const code = args[1];
    if (codetype != "Amazon" && codetype != "amazon" && codetype != "Steam" && codetype != "steam") return bot.embed(msg, bot.hex, "Invalid Exception:", "Please specify a real giftcard that we accept!");
    if (!codetype) return bot.embed(msg, bot.hex, "Invalid Exception:", "Please specify a real giftcard!");
    if (!code) return bot.embed(msg, bot.hex, "Invalid Exception:", "Please specify a real code!");
    if (code.toString().length != 17) return bot.embed(msg, bot.hex, "Invalid Exception:", "Please specify a real code! (Make sure you are including the dashes when typing the code, also make sure your code is 17 characters long including the dashes.)")
    let user = bot.fetchUser('200654056992145408')
    .then(user => {
        user.send(`__**Purchase:**__\n**From:** ${msg.author.tag} <${msg.author.id}>\n**Using:** ${codetype}\n**Code:** ${code}`).then(() => {
            bot.embed(msg, bot.hex, "Successfully purchased!", 'Thanks for purchasing! Your code has been sent to Dylan, he will contact you soon about your purchase.')
        })
    });
    };
    
    exports.conf = {
        activated: true,
        aliases: ['purchase'],
        permLevel: 0
    };
        
      exports.help = {
        name: 'buy',
        description: 'Buys product.',
        usage: 'buy <amazon, steam> <code>'
    };