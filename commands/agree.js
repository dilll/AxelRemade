exports.run = function(bot, msg) {
  if (msg.channel.type != "text") return;
    if (msg.guild.id != 200654805125955586) return bot.embed(msg, bot.hex, null, `This command is only usable in the XenMediaGroup Discord!`);
    if (msg.channel.id != 439141896603041814) return;
    msg.channel.bulkDelete(1, true);
    var role = msg.guild.roles.find('name', 'NotAgreed');
    msg.member.removeRole(role)
    msg.author.send({embed: {
    color: 0xff0000,
    author: {
      name: msg.client.user.username,
      icon_url: msg.client.user.avatarURL
    },
    title: "Thank you for agreeing to our rules!",
    description: "We hope you enjoy your time here!",
    timestamp: new Date(),
    footer: {
      icon_url: msg.client.user.avatarURL,
      text: "Agree Message"
    }
  }
});
}

exports.conf = {
    activated: true,
    aliases: [],
    permLevel: 1
  };
      
  exports.help = {
    name: 'agree',
    description: "Doing this command tells us you agree to our rules when you first join!",
    usage: 'agree'
  };