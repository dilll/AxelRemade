const Discord = require("discord.js")

exports.run = function(bot, msg) {
    msg.channel.send(`React with these emotes down below!`).then(function(message) {
    setTimeout(function(){ 
    message.react("◀")
    }, 500);
    setTimeout(function(){ 
    message.react("▶")
    }, 1500);
    setTimeout(function(){ 
    message.react("❌")
    }, 1000)
})
    
    const collector = msg.createReactionCollector((reaction, user) => 
    user.id === msg.author.id &&
    reaction.emoji.name === "◀" ||
    reaction.emoji.name === "▶" ||
    reaction.emoji.name === "❌"
).once("collect", reaction => {
    const chosen = reaction.emoji.name;
    if(chosen === "◀"){
        msg.edit("You have gone left!")
    }else if(chosen === "▶"){
        msg.edit("You have gone right!")
    }else{
        msg.edit("You have left this menu!")
    }
    collector.stop();
});
};

exports.conf = {
  activated: true,
  aliases: [],
  permLevel: 10
};
  
exports.help = {
  name: 'testing2',
  description: 'idek tbh just testing stuff',
  usage: 'testing2'
};