exports.run = function(bot, msg) {
  const totalPlaylists = bot.playlists.array().reduce((prev, curr) => prev + curr.queue.length, 0);
  const totalGuilds = bot.playlists.array().filter(g => !!g.dispatcher).length;
  bot.embed(msg, bot.hex, "Fetched statistical data:", `Currently queuing a total of ${totalPlaylists} songs on ${totalGuilds} servers, for a total of ${totalPlaylists} songs.`);
};

exports.conf = {
    activated: true,
    aliases: [],
    permLevel: 0
  };
  
  exports.help = {
    name: 'stats',
    description: 'Displays stats for the bot.',
    usage: 'stats'
  };