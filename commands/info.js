exports.run = function(bot, msg) {
    msg.channel.send({embed: {
    color: 0xff0000,
    author: {
      name: msg.author.username,
      icon_url: msg.author.avatarURL
    },
    title: "About Axel!",
    description: "Hi, my name is Axel and I am a Discord bot created by Dilll#0001! I have lots of great features such as: ",
    fields: [{
        name: "Built-in music bot!",
        value: "I have my very own music bot built in!"
      },
      {
        name: "Quick Updates and Bug Fixes.",
        value: "My owner Dylan is very quick at updating me and fixing me so I can be perfect for any server!"
      },
      {
        name: "Very Quick and 97% Uptime.",
        value: "Dylan made me using Discord.JS and I am very fast with a 97% uptime! Here is my uptime page: https://uptime.axelbot.ga"
      }
    ],
    timestamp: new Date(),
    footer: {
      text: "Command executed!"
    }
  }
});
};

exports.conf = {
    activated: true,
    aliases: [],
    permLevel: 0
  };
  
  exports.help = {
    name: 'info',
    description: 'Info about Axel!',
    usage: 'info'
  };